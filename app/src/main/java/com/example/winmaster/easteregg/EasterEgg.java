package com.example.winmaster.easteregg;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Shader;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

public class EasterEgg extends AppCompatActivity
{
    TextView textViewEaster;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easter_egg);
        textViewEaster = findViewById(R.id.textViewEaster);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.egg_texture);
        Shader shader = new BitmapShader(bitmap,Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        textViewEaster.getPaint().setShader(shader);
        textViewEaster.setShadowLayer(10, 3, 3, 0xFFFF8055);
    }
}
