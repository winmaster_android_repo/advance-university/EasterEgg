package com.example.winmaster.easteregg;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.WindowManager;

import java.util.Random;

/**
 * Created by winmaster on 26.03.2018.
 */

public class EasterWallpaper extends WallpaperService
{
    int screenHeight;
    int screenWidth;
    private final Handler handler = new Handler();
    Random random;

    Context context;

    public void onCreate()
    {
        context = this;
        super.onCreate();
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public Engine onCreateEngine()
    {
        random = new Random();
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        screenHeight = metrics.heightPixels;
        screenWidth = metrics.widthPixels;

        return new TheEngine();
    }


    public class TheEngine extends WallpaperService.Engine
    {
        final Runnable mDrawFrame = new Runnable()
        {
            @Override
            public void run()
            {
                drawFrame();
            }
        };

        private int egg1_y = 0;
        private int egg2_y =0;
        private int egg3_y = 0;
        private int egg4_y = 0;

        private int counter = 0;

        private float x;
        private float y;
        private boolean shouldDrawOpening = false;

        private int controlEggYPosition(Bitmap bitmap, int div, int counterDiv)
        {
            // 1/4 wysokosci grafiki to maksymalne wychylenie
            int maxTilt = bitmap.getHeight() / 4;
            int step = maxTilt / div;
            //wychylenie bedzie cykliczne gora-dol , funkcja sinus
            return Math.sin(counter/counterDiv) > 0 ? -step : step;
        }

        private int setCoordinateX(Bitmap bitmap)
        {
            return random.nextInt(screenWidth - bitmap.getWidth());
        }

        private int setCoordinateY(Bitmap bitmap)
        {
            return  random.nextInt(screenHeight - bitmap.getHeight());
        }

        @Override
        public void onTouchEvent(MotionEvent motionEvent)
        {
            int action = motionEvent.getAction();
            shouldDrawOpening = (action == MotionEvent.ACTION_DOWN ||
                    action == MotionEvent.ACTION_MOVE);
            x = motionEvent.getX();
            y = motionEvent.getY();
            super.onTouchEvent(motionEvent);
        }

        private void drawFrame()
        {
            counter++;

            final SurfaceHolder surfaceHolder = getSurfaceHolder();
            Canvas canvas = surfaceHolder.lockCanvas();
            canvas.save();

            //tło
            canvas.drawColor(ContextCompat.getColor(getApplicationContext(), R.color.background));

            //trawa i kroliki
            Bitmap grass = BitmapFactory.decodeResource(getResources(), R.drawable.grass);
            Bitmap rabbits = BitmapFactory.decodeResource(getResources(), R.drawable.rabbits);
            Shader shader = new BitmapShader(rabbits,Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            Paint paint = new Paint();
            canvas.drawBitmap(grass, ((canvas.getWidth()  - grass.getWidth()) /2), (canvas.getHeight()  - grass.getHeight()), paint);
            paint.setShader(shader);

            if (shouldDrawOpening)
            {
                canvas.drawCircle(x, y - 50, 100, paint);
            }

            //jajka

            Bitmap egg1 = BitmapFactory.decodeResource(getResources(), R.drawable.egg1);
            canvas.drawBitmap(egg1, 0, egg1_y, new Paint());
            egg1_y += controlEggYPosition( egg1, 20, 2);

            Bitmap egg3 = BitmapFactory.decodeResource(getResources(), R.drawable.egg3);
            canvas.drawBitmap(egg3, ((screenWidth - egg3.getWidth()) /4), egg3_y, new Paint());
            egg3_y+= controlEggYPosition(egg3, 10, 3);

            Bitmap egg2 = BitmapFactory.decodeResource(getResources(), R.drawable.egg2);
            canvas.drawBitmap(egg2, ((screenWidth - egg2.getWidth()) /2), egg2_y,new Paint());
            egg2_y+= controlEggYPosition(egg2, 25, 1);

            Bitmap egg4 = BitmapFactory.decodeResource(getResources(), R.drawable.egg4);
            canvas.drawBitmap(egg4, ((screenWidth - egg4.getWidth())), egg4_y,new Paint());
            egg4_y+= controlEggYPosition(egg4, 15, 4);


            //motylki
            Bitmap butterfly1 = BitmapFactory.decodeResource(getResources(), R.drawable.butterfly1);
            Bitmap butterfly2 = BitmapFactory.decodeResource(getResources(), R.drawable.butterfly2);
            Bitmap butterfly3 = BitmapFactory.decodeResource(getResources(), R.drawable.butterfly3);

            if(random.nextInt(100)%5==0)
            {
                int imgNumber = random.nextInt(3);

                switch (imgNumber)
                {
                    case 0:
                    {
                        canvas.drawBitmap(butterfly1, setCoordinateX(butterfly1), setCoordinateY(butterfly1), new Paint());
                        break;
                    }
                    case 1:
                    {
                        canvas.drawBitmap(butterfly2, setCoordinateX(butterfly2), setCoordinateY(butterfly2), new Paint());
                        break;
                    }
                    case 2:
                    {
                        canvas.drawBitmap(butterfly3, setCoordinateX(butterfly3), setCoordinateY(butterfly3), new Paint());
                        break;
                    }
                }
            }

            //sterowanie
            canvas.restore();
            surfaceHolder.unlockCanvasAndPost(canvas);

            handler.postDelayed(mDrawFrame,15);
        }

        public void onVisibilityChanged(boolean isVisible)
        {
            if(isVisible)
            {
                handler.post(mDrawFrame);
            }
            else
            {
                handler.removeCallbacks(mDrawFrame);
            }
        }
    }
}
