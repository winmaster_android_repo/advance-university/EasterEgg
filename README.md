# Animated wallpaper app with wallpaper service related to Easter holiday

![](screens/app.gif)

# Technical aspects:
## Creating custom views (rotating ImageView) and implementing custom UI elements, animations on canvas, using trigonometric functions to control bitmaps, making shaders, animating texts, peek through the image effect.

# Get it on Google Play!
<a href='https://play.google.com/store/apps/details?id=winmaster.easteregg&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img width="250" height="100" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>
![](screens/easter-app-promotion.png)

## Screenshots

![](screens/screen1.png) ![](screens/screen2.png) ![](screens/screen3.png) ![](screens/screen4.png)


## Overview from real phone

![](screens/presentation.gif)
